import unittest

import click
import click.testing

from incubator.cli.types import KeyValue, MountVolume


class TestCliTypes(unittest.TestCase):
    def setUp(self):
        self.runner = click.testing.CliRunner()

    def test_key_value(self):
        @click.command()
        @click.option('--kv', type=KeyValue())
        def copy(kv):
            k, v = kv
            click.echo("k:{}\n"
                       "v:{}".format(k, v))

        result = self.runner.invoke(copy, ['--kv', 'key=value'])
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, "k:key\nv:value\n")

        result = self.runner.invoke(copy, ['--kv', 'key-value'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['--kv', 'key:value'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['--kv', 'keyvalue'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['--kv', 'key=value=value'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['--kv', 'key='])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['--kv', '=key'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['--kv', '=key='])
        self.assertNotEqual(result.exit_code, 0)

    def test_volume(self):
        @click.command()
        @click.option('-v', type=MountVolume())
        def copy(v):
            click.echo("s:{}\n"
                       "d:{}\n"
                       "m:{}".format(v.source,
                                     v.destination,
                                     v.mode))

        result = self.runner.invoke(copy, ['-v', 'source:destination'])
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, "s:source\nd:destination\nm:None\n")

        result = self.runner.invoke(copy, ['-v', 'source:destination:ro'])
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output,
                         "s:source\nd:destination\nm:ro\n")

        result = self.runner.invoke(copy, ['-v', 'a/b/c:/d/e/f:rw'])
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, "s:a/b/c\nd:/d/e/f\nm:rw\n")

        result = self.runner.invoke(copy, ['-v', 'a/b/c:'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['-v', ':a/b/c:'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['-v', 'a:b:'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['-v', ':a:b'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['-v', 'a:b:r'])
        self.assertNotEqual(result.exit_code, 0)

        result = self.runner.invoke(copy, ['-v', 'a:b:write'])
        self.assertNotEqual(result.exit_code, 0)


if __name__ == '__main__':
    unittest.main()
