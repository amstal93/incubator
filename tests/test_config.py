"""
Tests for Config class.
"""

import unittest

from incubator.core.config import ImageConfig
from incubator.core.constants import (CONFIG_BUILDARGS,
                                      CONFIG_CONTAINER_LIMITS,
                                      CONFIG_CONTEXT_FILE_LIMIT,
                                      CONFIG_FORCE_RM, CONFIG_INFINITE_COMMAND,
                                      CONFIG_LABELS, CONFIG_MKDIR_COMMAND,
                                      CONFIG_PULL, CONFIG_RM, CONFIG_TAGS,
                                      CONFIG_VOLUMES,
                                      DEFAULT_CONTEXT_FILE_LIMIT,
                                      DEFAULT_FORCE_RM,
                                      DEFAULT_INFINITE_COMMAND,
                                      DEFAULT_MKDIR_COMMAND, DEFAULT_PULL,
                                      DEFAULT_RM)
from incubator.core.utilclasses import Volume

DICTIONARY_PROPERTIES = [CONFIG_BUILDARGS, CONFIG_CONTAINER_LIMITS,
                         CONFIG_LABELS]

LIST_PROPERTIES = [CONFIG_TAGS]

VALUE_PROPERTIES = [CONFIG_CONTEXT_FILE_LIMIT, CONFIG_FORCE_RM, CONFIG_INFINITE_COMMAND,
                    CONFIG_MKDIR_COMMAND, CONFIG_PULL, CONFIG_RM]
VALUE_PROPERTIES_NUMBER = [CONFIG_CONTEXT_FILE_LIMIT]
VALUE_PROPERTIES_BOOLEAN = [CONFIG_FORCE_RM, CONFIG_PULL, CONFIG_FORCE_RM]
VALUE_PROPERTIES_STRING = [CONFIG_INFINITE_COMMAND, CONFIG_MKDIR_COMMAND]


class TestConfig(unittest.TestCase):
    def setUp(self):
        pass

    def test_default_values(self):
        empty_config = ImageConfig(config_dict={})

        self.assertEqual(empty_config.buildargs, {})
        self.assertEqual(empty_config.container_limits, {})
        self.assertEqual(empty_config.context_file_limit, DEFAULT_CONTEXT_FILE_LIMIT)
        self.assertEqual(empty_config.forcerm, DEFAULT_FORCE_RM)
        self.assertEqual(empty_config.infinite_command, DEFAULT_INFINITE_COMMAND)
        self.assertEqual(empty_config.labels, {})
        self.assertEqual(empty_config.layers, [])
        self.assertEqual(empty_config.mkdir_command, DEFAULT_MKDIR_COMMAND)
        self.assertEqual(empty_config.pull, DEFAULT_PULL)
        self.assertEqual(empty_config.rm, DEFAULT_RM)
        self.assertEqual(empty_config.tags, [])
        self.assertEqual(empty_config.volumes, [])

    def test_add(self):
        for d in DICTIONARY_PROPERTIES:
            i1 = ImageConfig(config_dict={d: {"key": "value"}})
            i2 = ImageConfig(config_dict={d: {"key2": "value2"}})
            i_add = i1 + i2
            i_add_reverse = i2 + i1
            self.assertEqual(i_add.config[d], {
                "key": "value",
                "key2": "value2"
            })

            self.assertEqual(i_add_reverse.config[d], {
                "key": "value",
                "key2": "value2"
            })

    def test_add_empty(self):
        for d in DICTIONARY_PROPERTIES:
            i1 = ImageConfig(config_dict={})
            i2 = ImageConfig(config_dict={})
            i3 = ImageConfig(config_dict={d: {"key": "value"}})
            i4 = ImageConfig(config_dict={d: {}})

            i_add1 = i1 + i2
            self.assertEqual(i_add1.config.get(d), None)

            i_add2 = i1 + i3
            self.assertEqual(i_add2.config.get(d), {
                "key": "value"
            })

            i_add3 = i3 + i1
            self.assertEqual(i_add3.config.get(d), {
                "key": "value"
            })

            i_add4 = i1 + i4
            self.assertEqual(i_add4.config.get(d), {})
            i_add4_reverse = i4 + i1
            self.assertEqual(i_add4_reverse.config.get(d), {})

            i_add5 = i3 + i4
            self.assertEqual(i_add5.config.get(d), {
                "key": "value"
            })
            i_add5_reverse = i4 + i3
            self.assertEqual(i_add5_reverse.config.get(d), {
                "key": "value"
            })

    def test_add_override(self):
        for d in DICTIONARY_PROPERTIES:
            i1 = ImageConfig(config_dict={d: {"key": "value1"}})
            i2 = ImageConfig(config_dict={d: {"key": "value2"}})

            i_add = i1 + i2
            self.assertEqual(i_add.config.get(d), {
                "key": "value2"
            })

            i_add_reverse = i2 + i1
            self.assertEqual(i_add_reverse.config.get(d), {
                "key": "value1"
            })

    def test_add_list(self):
        for l in LIST_PROPERTIES:
            i1 = ImageConfig(config_dict={l: ["value1"]})
            i2 = ImageConfig(config_dict={l: ["value2"]})

            i_add = i1 + i2
            self.assertSetEqual(set(i_add.config[l]), {"value1", "value2"})
            i_add_reverse = i2 + i1
            self.assertSetEqual(set(i_add_reverse.config[l]), {"value1", "value2"})

            i3 = ImageConfig(config_dict={l: ["value0", "value1"]})
            i_add_same = i1 + i3
            self.assertSetEqual(set(i_add_same.config[l]), {"value0", "value1"})

            i_add_same_reverse = i3 + i1
            self.assertSetEqual(set(i_add_same_reverse.config[l]), {"value0", "value1"})

    def test_add_volume(self):

        value1 = Volume.get_instance("source:destination1")
        value2 = Volume.get_instance("source:destination2")
        value3 = Volume.get_instance("source:destination3")

        l = CONFIG_VOLUMES
        i1 = ImageConfig(config_dict={l: [str(value1)]})
        i2 = ImageConfig(config_dict={l: [str(value2)]})

        i_add = i1 + i2
        self.assertSetEqual(set(i_add.config[l]), {value1, value2})

        i_add_reverse = i2 + i1
        self.assertSetEqual(set(i_add_reverse.config[l]), {value1, value2})

        i3 = ImageConfig(config_dict={l: [str(value3), str(value1)]})
        i_add_same = i1 + i3
        self.assertSetEqual(set(i_add_same.config[l]), {value1, value3})

        i_add_same_reverse = i3 + i1
        self.assertSetEqual(set(i_add_same_reverse.config[l]), {value1, value3})

    def test_add_layers(self):
        i1 = ImageConfig()
        i1.layers = []
        i2 = ImageConfig()
        i2.layers = []

        i_add = i1 + i2
        self.assertEqual(i_add.layers, [])

        i3 = ImageConfig()
        i3.layers = [0]

        id_add_zero = i1 + i3
        self.assertEqual(id_add_zero.layers, [0])

        id_add_zero_reverse = i3 + i1
        self.assertEqual(id_add_zero_reverse.layers, [])

        i4 = ImageConfig()
        i4.layers = [1, 4, 6]

        i5 = ImageConfig()
        i5.layers = [3, 4, 7]

        i_add_different = i4 + i5
        self.assertEqual(i_add_different.layers, i5.layers)

        i_add_different_reverse = i5 + i4
        self.assertEqual(i_add_different_reverse.layers, i4.layers)

    def test_layers(self):

        self.assertTrue(ImageConfig.validate_json_schema(config={
            "version": 1.0,
            "layers": [0, 1, 2]
        }))

        i = ImageConfig(config_dict={
            "version": 1.0,
            "layers": [2, 10, 5]
        })
        self.assertEqual(i.layers, [2, 5, 10], "Final layers have to be ordered.")

        i2 = ImageConfig(config_dict={
            "version": 1.0,
            "layers": [2, 1, 1, 2, 10, 10, 5]
        })
        self.assertEqual(i2.layers, [1, 2, 5, 10], "Final layers have to be unique.")

    def test_dict_properties(self):
        for d in DICTIONARY_PROPERTIES:
            self.assertEqual(
                ImageConfig(config_dict={
                    "version": 1.0,
                    d: {
                        "name": "test_name"
                    }
                }).config[d],
                {"name": "test_name"})

            self.assertEqual(
                ImageConfig(config_dict={
                    "version": 1.0,
                    d: {
                        "name1": "value1",
                        "name2": "value2"
                    }
                }).config[d],
                {
                    "name1": "value1",
                    "name2": "value2"
                })

            i_udpate = ImageConfig(config_dict={
                "version": 1.0,
                d: {
                    "name1": "value1",
                    "name2": "value2"
                }
            })
            i_udpate.update(**{d: {"name3": "value3"}})

            self.assertEqual(i_udpate.config[d],
                             {
                                 "name1": "value1",
                                 "name2": "value2",
                                 "name3": "value3"
                             })

            i_udpate2 = ImageConfig(config_dict={
                "version": 1.0,
                d: {
                    "name1": "value1",
                    "name2": "value2"
                }
            })
            i_udpate2.update(**{d: {"name2": "value3"}})

            self.assertEqual(i_udpate2.config[d],
                             {
                                 "name1": "value1",
                                 "name2": "value3"
                             })

    def test_value_properties(self):
        for val1, val2, properties in [(True, False, VALUE_PROPERTIES_BOOLEAN),
                                       (1, 2, VALUE_PROPERTIES_NUMBER),
                                       ("val1", "val2", VALUE_PROPERTIES_STRING)]:
            for v in properties:
                self.assertEqual(
                    ImageConfig(config_dict={v: val1}).config[v], val1)

                i_update = ImageConfig()
                i_update.update(**{v: val1})

                self.assertEqual(i_update.config[v], val1)

                i_update2 = ImageConfig(config_dict={v: val1})
                i_update2.update(**{v: val2})

                self.assertEqual(i_update2.config[v], val2)

    def test_validation(self):

        self.assertTrue(ImageConfig.validate_json_schema(config={
            "version": 1.0,
            "buildargs": {
                "a": "b",
                "c": "d"
            },
            "container_limits": {
                "memory": "b",
                "memswap": "d",
                "cpushares": "e",
                "cpusetcpus": "f"
            },
            "context_file_limit": -1,
            "forcerm": True,
            "infinite_command": "cat -- ",
            "labels": {
                "name1": "value1",
                "name2": "value2"
            },
            "layers": [0, 1, 2],
            "mkdir_command": "mkdir --parent",
            "pull": False,
            "rm": True,
            "tags": [
                "registry.com/my_project:version",
                "my_project:version",
                "project",
                "myrepository.com/project"
            ],
            "volumes": [
                "/srv/data/test/webapp/secret:/usr/share/webapp/secret",
                "/srv/data/test/webapp2/secret:/usr/share/webapp2/secret"
            ]
        }))

    def test_validation_version(self):

        self.assertTrue(ImageConfig.validate_json_schema(
            config={"version": 1.0}))

        self.assertFalse(ImageConfig.validate_json_schema(
            config={"version": "1.0"}))

        self.assertFalse(ImageConfig.validate_json_schema(
            config={"version": [1, 0]}))

    def test_validation_dicts(self):
        for d in DICTIONARY_PROPERTIES:
            self.assertTrue(ImageConfig.validate_json_schema({
                "version": 1.0,
                d: {
                    "name": "test_name"
                }
            }))

            self.assertTrue(ImageConfig.validate_json_schema({
                "version": 1.0,
                d: {"name1": "value1",
                    "name2": "value2"
                    }
            }))

            self.assertFalse(ImageConfig.validate_json_schema(
                config={d: "value"}))

            self.assertFalse(ImageConfig.validate_json_schema(
                config={d: ["a", "b"]}))

            self.assertFalse(ImageConfig.validate_json_schema(
                config={d: {"a", 1}}))

    def test_validation_arrays(self):
        for l in LIST_PROPERTIES + [CONFIG_VOLUMES]:
            self.assertTrue(ImageConfig.validate_json_schema({
                "version": 1.0, l: []}))

            self.assertTrue(ImageConfig.validate_json_schema({
                "version": 1.0, l: ["val:ue1", "val:ue2"]}))

            self.assertFalse(ImageConfig.validate_json_schema(
                config={l: "val:ue"}))

            self.assertFalse(ImageConfig.validate_json_schema(
                config={l: {"a": "b"}}))

            self.assertFalse(ImageConfig.validate_json_schema(
                config={l: {"a", 1}}))

    def test_name(self):
        i1 = ImageConfig()
        self.assertEqual(i1.name, [])
        i2 = ImageConfig(config_dict={"config_name": "name"})
        self.assertEqual(i2.name, ["name"])

        i_add = i1 + i2
        self.assertEqual(i_add.name, ["name"])

        i3 = ImageConfig(config_dict={"config_name": "name2"})
        i_add2 = i2 + i3
        self.assertEqual(i_add2.name, ["name", "name2"])
        i_add2_reverse = i3 + i2
        self.assertEqual(i_add2_reverse.name, ["name2", "name"])


if __name__ == '__main__':
    unittest.main()
