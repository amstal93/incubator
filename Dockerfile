FROM fedora:25
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN pip3 install ipython
COPY . /app/
RUN pip3 install /app
RUN bash -c " cat /app/.bash_eval >> ~/.bashrc "
RUN rm -rf /app
CMD /bin/bash