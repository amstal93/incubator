If you are familiar with `Docker-py`, then you would probably like the `build` function from *Incubator*. It can replace the `build` function from `Docker-py` but has some extended atributes.

Firstly, you have to import incubator module:

```python
from incubatori.api import build
```

Now you can start the build in teh current directory:
```python
result = build(path=".")
```

The result contains id of the created image:

```
result.id

```

It also contains logs:

```
for cmd_log in result.logs:
    print(cmd_log.logs)
```


## Dockerfile in memory

You can also define your Dockerfile (and context) in memory:

```python
In [1]: from incubator.api import build

In [2]: import six

In [3]: dockerfile = six.BytesIO("FROM fedora:25\nRUN echo \"hello\"\nRUN echo \"world\"".encode())

In [4]: result = build(fileobj=dockerfile)

In [5]: result.id
Out[5]: 'sha256:2d84f4a77bc35c1d484a146a5107408240ce65a23a30208c45c87affdba02d8a'

In [6]: for cmd_log in result.logs:
   ...:     print(cmd_log.logs)
   ...:     
['hello']
['world']
```
