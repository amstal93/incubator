For building the image, you can use the same `Dockerfile` as with the `docker build`, the extended functionality can be defined as an option or in the configuration file(s).

Let us start with the basic example:


## Dockerfile:

There is an example Dockerfile created for you. You can see it in the terminal or here:

```dockerfile
FROM fedora:25
ENV KEY=VALUE
RUN touch /tmp/a
LABEL AUTHOR="me"
RUN echo "hello world!"
```   

## Run the build process:

You can start the build proccess with the following command:

`incubator build`

> If you do not specify any argument, the build will take the current working directory as a build context.

It can take some time for the first time. (The Docker needs to download fedora image.)

## Inspect the result

To get some information about image, you can use `docker inspect` or `docker history`. We can copy the `id` of the image and use the two following commands:

**Inspect the image:**

`docker inspect pasted_id`

**Get the image layering:**

`docker history pasted_id`


## Get output

If you want to see more information during the build (e.g. command outputs), you can use the `--verbose` flag:

`incubator build --verbose`




